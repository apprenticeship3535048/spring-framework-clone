package sql;

import annotations.Column;
import entities.Entity;
import magic.Magician;
import settings.ApplicationProps;
import settings.ApplicationPropsService;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.*;

public class Connector {

    private final ApplicationPropsService service;
    private final Connection connection;

    public Connector(ApplicationPropsService service) throws SQLException, IOException, ClassNotFoundException {
        this.service = service;
        this.connection = establishConnection();
    }

    public Connection establishConnection() throws IOException, SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        ApplicationProps props = service.getMySqlData();
        return DriverManager.getConnection(props.getConnection(), props.getUsername(), props.getPassword());
    }

    public <T extends Entity> ResultSet executeQuery(Class<T> clazz) throws SQLException {
        Statement statement = connection.createStatement();

        List<Field> fields = Magician.getAllFields(clazz);
        List<String> fieldNames = new ArrayList<>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                Column annotation = field.getAnnotation(Column.class);
                String columnName = annotation.name().isEmpty() ? field.getName() : annotation.name();
                fieldNames.add(columnName);
            }
        }
        String query = "SELECT " + String.join(", ", fieldNames) + " FROM answer";

        return statement.executeQuery(query);
    }
}
