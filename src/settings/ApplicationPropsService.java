package settings;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationPropsService {

    private final Properties props = new Properties();

    public ApplicationProps getMySqlData() throws IOException {
        String propsPath = "application.properties";
        FileInputStream fileInputStream = new FileInputStream(propsPath);
        props.load(fileInputStream);
        return new ApplicationProps(
                props.getProperty("connection"),
                props.getProperty("username"),
                props.getProperty("password")
        );
    }
}
