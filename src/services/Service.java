package services;

import entities.Entity;
import magic.Magician;
import sql.Connector;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Service<T extends Entity> {

    private final Connector connector;
    public Class<T> type;

    public Service(Connector connector, Class<T> type) {
        this.connector = connector;
        this.type = type;
    }


    public List<T> findAll() throws SQLException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException {
        ResultSet resultSet = connector.executeQuery(getType());
        List<T> resultList = new ArrayList<>();
        while (resultSet.next()) {
            Map<String, Object> results = new HashMap<>();
            for (int i = 1; i < resultSet.getMetaData().getColumnCount() + 1; i++) {
                String columnName = resultSet.getMetaData().getColumnName(i);
                Object columnValue = resultSet.getObject(i);
                results.put(columnName, columnValue);
            }
            T entity = type.getDeclaredConstructor().newInstance();
            T entity1 = Magician.buildEntityFromQueryResult(results, entity);
            resultList.add(entity1);
        }
        return resultList;
    }

    public Class<T> getType() {
        return type;
    }
}
