package magic;

import annotations.Column;
import entities.Entity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class Magician {

    public static <T extends Entity> T buildEntityFromQueryResult(Map<String, Object> result, T entity) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        List<Field> fields = getAllFields(entity.getClass());
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                Method setter = getSetter(field, entity);
                setter.setAccessible(true);
                Object obj = result.get(field.getName());
                if (field.getType() == UUID.class) {
                    UUID uuid = getUUIDFromBytes((byte[]) obj);
                    setter.invoke(entity, uuid);
                } else setter.invoke(entity, obj);
            }
        }
        return entity;
    }

    public static List<Field> getAllFields(Class<?> clazz) {
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> inter : interfaces) {
            fields.addAll(Arrays.asList(inter.getDeclaredFields()));
        }
        Class<?> superClass = clazz.getSuperclass();
        if (null != superClass) {
            fields.addAll(getAllFields(superClass));
        }
        return fields;
    }

    private static <T> Method getSetter(Field field, T entity) throws NoSuchMethodException {
        String setterName = "set" + capitalize(field.getName());
        return entity.getClass().getMethod(setterName, field.getType());
    }
    private static String capitalize(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }

    private static UUID getUUIDFromBytes(byte[] bytes) {
        if (bytes.length != 16) {
            throw new IllegalArgumentException("Not a UUID");
        }
        long mostSigBits = 0;
        long leastSigBits = 0;
        for (int i = 0; i < 8; i++) {
            mostSigBits = (mostSigBits << 8) | (bytes[i] & 0xff);
        }
        for (int i = 8; i < 16; i++) {
            leastSigBits = (leastSigBits << 8) | (bytes[i] & 0xff);
        }
        return new UUID(mostSigBits, leastSigBits);
    }
}
