import entities.Answer;
import entities.Entity;
import services.Service;
import settings.ApplicationPropsService;
import sql.Connector;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class RestApi {

    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException {

        ApplicationPropsService applicationPropsService = new ApplicationPropsService();
        Connector connector = new Connector(applicationPropsService);
        Service<Answer> service = new Service<Answer>(connector, Answer.class);
        List<Answer> list = service.findAll();
        for (Answer answer : list) {
            System.out.println(answer);
        }

    }
}